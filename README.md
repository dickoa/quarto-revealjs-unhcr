# quarto-revealjs-unhcr


## Usage

```sh
quarto install extension https://gitlab.com/dickoa/quarto-revealjs-unhcr/-/archive/main/quarto-revealjs-unhcr-main.zip
```


## Usage remarks

- Columns **may not sum up to 100% width** as some space is needed for the custom gray background around the columns. Each column must be reduced by 3% width, i.e. with two columns, the sum may only be 94% width.
- Level 1 headers (`#`) are reserved for "section slides", i.e. centered slides which only contain the title. These slides are then automatically added to the Table of Contents (if enabled).

## Screenshots
